'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('ContactCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:ProductsCtrl
 * @description
 * # ProductsCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('ProductsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

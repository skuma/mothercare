'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:FaqCollectionCtrl
 * @description
 * # FaqCollectionCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('FaqCollectionCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

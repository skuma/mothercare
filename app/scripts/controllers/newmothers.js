'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:NewmothersCtrl
 * @description
 * # NewmothersCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('NewmothersCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

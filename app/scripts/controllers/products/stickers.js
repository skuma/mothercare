'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:ProductsStickersCtrl
 * @description
 * # ProductsStickersCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('ProductsStickersCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:ProductsShowerCtrl
 * @description
 * # ProductsShowerCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('ProductsShowerCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:ProductsCakesCtrl
 * @description
 * # ProductsCakesCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('ProductsCakesCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

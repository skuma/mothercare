'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:ProductsShoesCtrl
 * @description
 * # ProductsShoesCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('ProductsShoesCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

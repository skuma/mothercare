'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

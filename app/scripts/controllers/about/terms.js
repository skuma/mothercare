'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:AboutTermsCtrl
 * @description
 * # AboutTermsCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('AboutTermsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

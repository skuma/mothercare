'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:AboutPrivacyCtrl
 * @description
 * # AboutPrivacyCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('AboutPrivacyCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

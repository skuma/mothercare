'use strict';

/**
 * @ngdoc function
 * @name motherCare.controller:EventsCtrl
 * @description
 * # EventsCtrl
 * Controller of the motherCare
 */
angular.module('motherCare')
  .controller('EventsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

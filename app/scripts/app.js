'use strict';

/**
 * @ngdoc overview
 * @name motherCare
 * @description
 * # motherCare
 *
 * Main module of the application.
 */
angular
  .module('motherCare', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .state('about', {
        url: '/about',
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .state('aboutTerms', {
        url: '/about/terms',
        templateUrl: 'views/about/terms.html',
        controller: 'AboutTermsCtrl'
      })
      .state('aboutPrivacy', {
        url: '/about/privacy',
        templateUrl: 'views/about/privacy.html',
        controller: 'AboutPrivacyCtrl'
      })
      .state('products', {
        url: '/products',
        templateUrl: 'views/products.html',
        controller: 'ProductsCtrl'
      })
      .state('productsBottoms', {
        url: '/products/bottoms',
        templateUrl: 'views/products/bottoms.html',
        controller: 'ProductsBottomsCtrl'
      })
      .state('productsCakes', {
        url: '/products/cakes',
        templateUrl: 'views/products/cakes.html',
        controller: 'ProductsCakesCtrl'
      })
      .state('productsNapkin', {
        url: '/products/napkin',
        templateUrl: 'views/products/napkin.html',
        controller: 'ProductsNapkinCtrl'
      })
      .state('productsShoes', {
        url: '/products/shoes',
        templateUrl: 'views/products/shoes.html',
        controller: 'ProductsShoesCtrl'
      })
      .state('productsShower', {
        url: '/products/shower',
        templateUrl: 'views/products/shower.html',
        controller: 'ProductsShowerCtrl'
      })
      .state('productsStickers', {
        url: '/products/stickers',
        templateUrl: 'views/products/stickers.html',
        controller: 'ProductsStickersCtrl'
      })
      .state('newmothers', {
        url: '/newmothers',
        templateUrl: 'views/newmothers.html',
        controller: 'NewmothersCtrl'
      })
      .state('newmothersUsefulsites', {
        url: '/newmothers/usefulsites',
        templateUrl: 'views/newmothers/usefulsites.html',
        controller: 'NewmothersUsefulsitesCtrl'
      })
      .state('newmothersPlanner', {
        url: '/newmothers/planner',
        templateUrl: 'views/newmothers/planner.html',
        controller: 'NewmothersPlannerCtrl'
      })
      .state('newmothersPlannerForm', {
        url: '/newmothers/planner/form',
        templateUrl: 'views/newmothers/planner/form.html',
        controller: 'NewmothersPlannerFormCtrl'
      })
      .state('newmothersBlog', {
        url: '/newmothers/blog',
        templateUrl: 'views/newmothers/blog.html',
        controller: 'NewmothersBlogCtrl'
      })
      .state('newmothersBlogPost1', {
        url: '/newmothers/blog/post1',
        templateUrl: 'views/newmothers/blog/post1.html',
        controller: 'NewmothersBlogPost1Ctrl'
      })
      .state('newmothersBlogPost2', {
        url: '/newmothers/blog/post2',
        templateUrl: 'views/newmothers/blog/post2.html',
        controller: 'NewmothersBlogPost2Ctrl'
      })
      .state('newmothersBlogPost3', {
        url: '/newmothers/blog/post3',
        templateUrl: 'views/newmothers/blog/post3.html',
        controller: 'NewmothersBlogPost3Ctrl'
      })
      .state('newmothersBlogPost4', {
        url: '/newmothers/blog/post4',
        templateUrl: 'views/newmothers/blog/post4.html',
        controller: 'NewmothersBlogPost4Ctrl'
      })
      .state('newmothersBlogPost5', {
        url: '/newmothers/blog/post5',
        templateUrl: 'views/newmothers/blog/post5.html',
        controller: 'NewmothersBlogPost5Ctrl'
      })
      .state('newmothersBlogPost6', {
        url: '/newmothers/blog/post6',
        templateUrl: 'views/newmothers/blog/post6.html',
        controller: 'NewmothersBlogPost6Ctrl'
      })
      .state('newmothersBlogPost7', {
        url: '/newmothers/blog/post7',
        templateUrl: 'views/newmothers/blog/post7.html',
        controller: 'NewmothersBlogPost7Ctrl'
      })
      .state('newmothersBlogPost8', {
        url: '/newmothers/blog/post8',
        templateUrl: 'views/newmothers/blog/post8.html',
        controller: 'NewmothersBlogPost8Ctrl'
      })
      .state('newmothersBlogPost9', {
        url: '/newmothers/blog/post9',
        templateUrl: 'views/newmothers/blog/post9.html',
        controller: 'NewmothersBlogPost9Ctrl'
      })
      .state('events', {
        url: '/events',
        templateUrl: 'views/events.html',
        controller: 'EventsCtrl'
      })
      .state('faq', {
        url: '/faq',
        templateUrl: 'views/faq.html',
        controller: 'FaqCtrl'
      })
      .state('faqCollection', {
        url: '/faq/collection',
        templateUrl: 'views/faq/collection.html',
        controller: 'FaqCollectionCtrl'
      })
      .state('faqDelivery', {
        url: '/faq/delivery',
        templateUrl: 'views/faq/delivery.html',
        controller: 'FaqDeliveryCtrl'
      })
      .state('faqParty', {
        url: '/faq/party',
        templateUrl: 'views/faq/party.html',
        controller: 'FaqPartyCtrl'
      })
      .state('contact', {
        url: '/contact',
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      });
  });

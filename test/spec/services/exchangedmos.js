'use strict';

describe('Service: exchangeDmos', function () {

  // load the service's module
  beforeEach(module('giiftbankApp'));

  // instantiate service
  var exchangeDmos;
  beforeEach(inject(function (_exchangeDmos_) {
    exchangeDmos = _exchangeDmos_;
  }));

  it('should do something', function () {
    expect(!!exchangeDmos).toBe(true);
  });

});

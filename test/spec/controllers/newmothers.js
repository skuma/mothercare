'use strict';

describe('Controller: NewmothersCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersCtrl = $controller('NewmothersCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersCtrl.awesomeThings.length).toBe(3);
  });
});

'use strict';

describe('Controller: FaqDeliveryCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var FaqDeliveryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FaqDeliveryCtrl = $controller('FaqDeliveryCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(FaqDeliveryCtrl.awesomeThings.length).toBe(3);
  });
});

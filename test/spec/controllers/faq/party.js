'use strict';

describe('Controller: FaqPartyCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var FaqPartyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FaqPartyCtrl = $controller('FaqPartyCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(FaqPartyCtrl.awesomeThings.length).toBe(3);
  });
});

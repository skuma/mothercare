'use strict';

describe('Controller: FaqCollectionCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var FaqCollectionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FaqCollectionCtrl = $controller('FaqCollectionCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(FaqCollectionCtrl.awesomeThings.length).toBe(3);
  });
});

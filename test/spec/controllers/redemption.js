'use strict';

describe('Controller: RedemptionCtrl', function () {

  // load the controller's module
  beforeEach(module('giiftbankApp'));

  var RedemptionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RedemptionCtrl = $controller('RedemptionCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RedemptionCtrl.awesomeThings.length).toBe(3);
  });
});

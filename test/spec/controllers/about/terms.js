'use strict';

describe('Controller: AboutTermsCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var AboutTermsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AboutTermsCtrl = $controller('AboutTermsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AboutTermsCtrl.awesomeThings.length).toBe(3);
  });
});

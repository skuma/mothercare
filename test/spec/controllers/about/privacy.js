'use strict';

describe('Controller: AboutPrivacyCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var AboutPrivacyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AboutPrivacyCtrl = $controller('AboutPrivacyCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AboutPrivacyCtrl.awesomeThings.length).toBe(3);
  });
});

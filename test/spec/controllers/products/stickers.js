'use strict';

describe('Controller: ProductsStickersCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var ProductsStickersCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductsStickersCtrl = $controller('ProductsStickersCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProductsStickersCtrl.awesomeThings.length).toBe(3);
  });
});

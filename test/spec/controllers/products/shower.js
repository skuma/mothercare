'use strict';

describe('Controller: ProductsShowerCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var ProductsShowerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductsShowerCtrl = $controller('ProductsShowerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProductsShowerCtrl.awesomeThings.length).toBe(3);
  });
});

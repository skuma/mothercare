'use strict';

describe('Controller: ProductsBottomsCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var ProductsBottomsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductsBottomsCtrl = $controller('ProductsBottomsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProductsBottomsCtrl.awesomeThings.length).toBe(3);
  });
});

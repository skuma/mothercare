'use strict';

describe('Controller: ProductsCakesCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var ProductsCakesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductsCakesCtrl = $controller('ProductsCakesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProductsCakesCtrl.awesomeThings.length).toBe(3);
  });
});

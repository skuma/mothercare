'use strict';

describe('Controller: ProductsShoesCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var ProductsShoesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductsShoesCtrl = $controller('ProductsShoesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProductsShoesCtrl.awesomeThings.length).toBe(3);
  });
});

'use strict';

describe('Controller: ProductsNapkinCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var ProductsNapkinCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductsNapkinCtrl = $controller('ProductsNapkinCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProductsNapkinCtrl.awesomeThings.length).toBe(3);
  });
});

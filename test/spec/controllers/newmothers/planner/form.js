'use strict';

describe('Controller: NewmothersPlannerFormCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersPlannerFormCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersPlannerFormCtrl = $controller('NewmothersPlannerFormCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersPlannerFormCtrl.awesomeThings.length).toBe(3);
  });
});

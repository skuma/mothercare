'use strict';

describe('Controller: NewmothersUsefulsitesCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersUsefulsitesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersUsefulsitesCtrl = $controller('NewmothersUsefulsitesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersUsefulsitesCtrl.awesomeThings.length).toBe(3);
  });
});

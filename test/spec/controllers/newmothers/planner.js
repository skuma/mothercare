'use strict';

describe('Controller: NewmothersPlannerCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersPlannerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersPlannerCtrl = $controller('NewmothersPlannerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersPlannerCtrl.awesomeThings.length).toBe(3);
  });
});

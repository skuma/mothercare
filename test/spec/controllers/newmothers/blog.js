'use strict';

describe('Controller: NewmothersBlogCtrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersBlogCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersBlogCtrl = $controller('NewmothersBlogCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersBlogCtrl.awesomeThings.length).toBe(3);
  });
});

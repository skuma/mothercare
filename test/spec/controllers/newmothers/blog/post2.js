'use strict';

describe('Controller: NewmothersBlogPost2Ctrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersBlogPost2Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersBlogPost2Ctrl = $controller('NewmothersBlogPost2Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersBlogPost2Ctrl.awesomeThings.length).toBe(3);
  });
});

'use strict';

describe('Controller: NewmothersBlogPost6Ctrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersBlogPost6Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersBlogPost6Ctrl = $controller('NewmothersBlogPost6Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersBlogPost6Ctrl.awesomeThings.length).toBe(3);
  });
});

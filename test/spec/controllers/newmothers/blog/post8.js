'use strict';

describe('Controller: NewmothersBlogPost8Ctrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersBlogPost8Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersBlogPost8Ctrl = $controller('NewmothersBlogPost8Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersBlogPost8Ctrl.awesomeThings.length).toBe(3);
  });
});

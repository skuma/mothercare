'use strict';

describe('Controller: NewmothersBlogPost5Ctrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersBlogPost5Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersBlogPost5Ctrl = $controller('NewmothersBlogPost5Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersBlogPost5Ctrl.awesomeThings.length).toBe(3);
  });
});

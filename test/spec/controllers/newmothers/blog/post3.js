'use strict';

describe('Controller: NewmothersBlogPost3Ctrl', function () {

  // load the controller's module
  beforeEach(module('motherCare'));

  var NewmothersBlogPost3Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewmothersBlogPost3Ctrl = $controller('NewmothersBlogPost3Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewmothersBlogPost3Ctrl.awesomeThings.length).toBe(3);
  });
});
